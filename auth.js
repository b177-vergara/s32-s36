const jwt = require("jsonwebtoken");

// User defined string data that will be used to create JSON web tokens
const secret = "CourseBookingAPI";

// Token creation
module.exports.createAccessToken = (user) => {
	const data = {
		id : user._id,
		email : user.email,
		isAdmin : user.isAdmin
	};

	return jwt.sign(data, secret, {});
}

// Token verification
module.exports.verify = (req, res, next) => {
	// the token is retrieved from the request header
	let token = req.headers.authorization;

	// Token received and is not undefined
	if(typeof token !== "undefined"){
		console.log(token);

		token = token.slice(7, token.length);

		// validate the token using "verify" method decrypting the token using the secret code
		return jwt.verify(token, secret, (err, data) => {
			// if JWT is not valid
			if(err){
				return res.send({auth : "failed"});
			}
			// if JWT is valid
			else{
				// Allows the application to proceed with the next middleware function/callback function in the route
				next();
			}
		})
	}
	// token does not exist
	else{
		return res.send({auth: "failed"});
	}
}

// Token decryption
module.exports.decode = (token) => {
	// token received and is not undefined
	if(typeof token !== "undefined"){
		// Retrieves only the token and removes the "Bearer" prefix
		token = token.slice(7, token.length);

		return	jwt.verify(token, secret, (err, data) => {
			if(err){
				return null;
			}
			else{
				// The "decode" method is used to obtain the information from JWT
				// The "{complete:true}" option allows us to return additional information from, JWT
				// The payload property contains the information provided in the "createAccessToken" method
				return jwt.decode(token, {complete:true}).payload
			}
		})
	}
	// Token does not exist
	else{
		return null;
	}
}