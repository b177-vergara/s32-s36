const Course = require("../models/Course");
const auth = require("../auth");

module.exports.addCourse = (reqBody) => {
    // Create a variable "newCourse" and instantiates a new "Course" object
    let newCourse = new Course({
        name : reqBody.name,
        description : reqBody.description,
        price : reqBody.price
    })

    return newCourse.save().then((course, error) => {
        if(error){
            return false;
        }
        else{
            return true;
        }
    })
}

module.exports.getAllCourses = () => {
    return Course.find({}).then(result => {
        return result;
    })
}

// Controller function for retreiving a specific course
module.exports.getCourse = (reqParams) => {
    return Course.findById(reqParams.courseId).then(result => {
        return result;
    })
}

// Controller function for updating a course
module.exports.updateCourse = (reqParams, reqBody) => {
    let updatedCourse = {
        name : reqBody.name,
        description : reqBody.description,
        price : reqBody.price
    }

    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
        if(error){
            return false;
        }
        else{
            return true;
        }
    })
}

// Controller function for archiving a course
module.exports.archiveCourse = (reqParams, reqBody) => {
    let updatedCourse = {
        isActive : reqBody.isActive
    }
    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
        if(error){
            return false;
        }
        else{
            return true;
        }
    })
}