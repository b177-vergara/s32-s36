const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");
// Route checking if the user's emai already exists in the database
// endpoint: localhost:4000/users/checkEmail
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for registering a user
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Activity
// Route for user details
router.get("/details", (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile(req.body).then(resultFromController => res.send(resultFromController));
})

// Route to enroll user to a course
router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		userId : req.body.userId,
		courseId : req.body.courseId
	}

	const userData = auth.decode(req.headers.authorization);

	userController.enroll(data).then(resultFromController => res.send(resultFromController));

})

module.exports = router;